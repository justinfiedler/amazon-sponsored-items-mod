# amazon-sponsored-items-mod
A Greasemonkey / Tampermonkey script that modifies sponsored and other unwanted content on Amazon

Sponsored items will be given a red border and background.

Currently supports:

* Sponsored search results
* Sponsored carousal items
* Sponsored Links
* Search feedback questionnaire

Thanks to Stefan-Code's [amazon-sponsored-items-blocker](https://github.com/Stefan-Code/amazon-sponsored-items-blocker) for the starting point of this script.
