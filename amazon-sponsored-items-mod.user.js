// ==UserScript==
// @name         Amazon sponsored items mod
// @namespace    https://bitbucket.org/justinfiedler/amazon-sponsored-items-mod
// @version      0.1
// @description  Modifies sponsored content on Amazon
// @author       Better World
// @include      *://www.amazon.com/*
// @include      *://smile.amazon.com/*
// @include      *://www.amazon.co.uk/*
// @include      *://www.amazon.de/*
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js
// @grant        none
// @run-at document-end
// ==/UserScript==

(function() {
    'use strict';

    var LOGGING_ENABLED = true;

    log("amazon-sponsored-items-mod loaded");

    var sponsoredCSS = {
        'border-radius': '5px',
        'border': '2px solid red',
        'padding': '5px',
        'background': 'rgba(255,0,0,0.2)'
    };

    var fakeSpotButtonWasAdded = false;

    var $ = jQuery.noConflict(true);

    // Keep track of the total count of sponsored items that we modify
    var sponsoredItemCount = 0;
    var incrementSponsoredItems = function () {
        ++sponsoredItemCount;
    };

    //debounce to prevent excessive updates
    var updateCallback = _.debounce(function() {
        removePageModifiedCallback();
        updateSponsoredAds();
        addPageModifiedCallback();
    }, 200, {
        'leading': false,
        'trailing': true,
        'maxWait': 1000
    });

    //Start listening to DOM modified events
    addPageModifiedCallback();

    function log() {
        if (LOGGING_ENABLED) {
            console.log.apply(console, arguments);
        }
    }

    // Adds callback to modify the sponsored items on page modified
    function addPageModifiedCallback() {
        $('body').bind("DOMSubtreeModified", updateCallback);
    }

    // Removed callback on page modified
    function removePageModifiedCallback() {
        $('body').unbind("DOMSubtreeModified", updateCallback);
    }

    function updateSponsoredAds() {
        sponsoredItemCount = 0;

        addFakeSpotButton();

        // Highlight
        combine(
            getSponsoredSearchItems(),
            getSponsoredCarousalItems()
        ).each(incrementSponsoredItems).css(sponsoredCSS);

        // Hide
        combine(
            getSponsoredLinks(),
            getSearchFeedback()
        ).each(incrementSponsoredItems).hide();

        log("amazon-sponsored-items-mod: " + sponsoredItemCount + " items updated!");
    }

    // Returns merged jQuery set of all given args
    // Combines all arguments using jQuery.add()
    function combine() {
        var i;
        var combined = $();
        for (i = 0; i < arguments.length; i++) {
            combined = combined.add(arguments[i]);
        }
        return combined;
    }

    // Returns Sponsored items in Search results
    function getSponsoredSearchItems() {
        return $('.celwidget').find(".s-sponsored-info-icon").closest('.celwidget');
    }

    // Returns Sponsored carousal items
    function getSponsoredCarousalItems() {
        // #sp_detail
        // .a-section.sp_offerVertical
        // .sponsored-products-truncator-truncated
        return $('.a-carousel-container').find(".sp_offerVertical").closest('.a-carousel-card');
    }

    function getSponsoredLinks() {
        return $('[id^="sponsoredLinks"]');
    }

    function getSearchFeedback() {
        return $('#hows-my-search');
    }

    function getTitleContainer() {
        return $('#title_feature_div');
    }

    function getShortProductUrl() {
        var productUrl = location.protocol + '//' + location.host + location.pathname;

        var index = productUrl.indexOf('/ref=');
        if (index > 0) {
            productUrl = productUrl.slice(0, index);
        }

        return productUrl;
    }

    function addFakeSpotButton() {
        const titleContainer = getTitleContainer();
        if (titleContainer && !fakeSpotButtonWasAdded) {
            fakeSpotButtonWasAdded = true;

            const productUrl = getShortProductUrl();
            const fakeSpotUrl = 'https://www.fakespot.com?ap=' + productUrl;

            const fakeSpotButtonBar = $('<div>')
                .addClass('amod-button-container')
                .append(
                    $('<button>')
                        .text('Fakespot')
                        .click(function() {
                            copyToClipboard(productUrl);
                            openInNewTab(fakeSpotUrl);
                        })
                )
                .append(
                    $('<button>')
                        .text('Copy Product URL')
                        .click(function() {
                            copyToClipboard(productUrl);
                        })
                )
                .css({
                    'margin': '5px'
                });

            fakeSpotButtonBar.find('button').css({
                'margin-right': '5px'
            });

            titleContainer.append(fakeSpotButtonBar);
        }
    }

    function copyToClipboard(text) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(text).select();
        document.execCommand("copy");
        $temp.remove();
    }

    function openInNewTab(url) {
        window.open(url,'_blank');
    }

    function hideAds() {
        // $(".slot__ad").hide();
        // $(".slot__feedback").hide();
        // $("[id*='ape_']").hide();
        // $("[id*='sponsored']").hide();
        // $("[id*='sp_']").hide();
        // $("[id*='advertising']").hide();
    }
})();